#!/bin/bash

log="validate_xml.log"
echo "# Log "`date` > $log

echo "# Vérification des fichiers xml pour les fictions narratives" | tee -a $log

inputDir="data/tei/*"

# Parcours des fichiers à transformer
for inputFile in $inputDir
do
  # Récupération du rng lié au fichier courant
  rng=$(cat $inputFile | grep -E '^<\?xml-model.*"(.*.rng)".*\?>$' | cut -d \" -f 2)
  # Validation
  jing $rng $inputFile 2>>$log
  echo "  - Input:  "$inputFile
  echo "  - Rng:  "$rng
  echo
done
echo

# compte les lignes (wc -l) du .log qui ne sont ni des warnings, ni des commentaires
# -E -> regex ; -v -> inverse de la recherche
cat validate_xml.log | grep -E -v '(^\[warning\]|^#)' 1>error_report.log
if [ $(cat error_report.log | wc -l) != 0 ]; then
  echo "  - Validation:  Error -> You can see errors in error_report.log"
  exit 1
else
  echo "  - Validation:  OK"
fi