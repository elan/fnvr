window.onload = function() {
  handler.init();
}

const handler = {
  init: function() {
    let modalCalls = document.querySelectorAll('.note-call');
    [].forEach.call(modalCalls, function(modalCall) {
      modalCall.addEventListener("click", handler.modalCall);
    });
  },
  modalCall: function(evt) {
    evt.stopPropagation();
    let modalCall = evt.currentTarget;
    let modalId = modalCall.dataset.modalId;
  
    let modalNb = document.querySelector('[note-title="' + modalId + '"]').innerHTML;
    let modalText = document.querySelector('[note-content="' + modalId + '"]').innerHTML;
    document.querySelector("#note .note-content").innerHTML = modalText;
    document.querySelector("#note-title-id").innerHTML = modalNb;
  
    new bootstrap.Modal(document.getElementById("note"), {}).toggle();
  }
}