# FNVR

## Installation

```bash
git clone git@gricad-gitlab.univ-grenoble-alpes.fr:elan/fnvr.git
cd fnvr
```

## Transformation des données en local (facultatif)

### Pré-requis

Installer saxon (car utilisé par le script bash ci-dessous) :
- `apt install default-jre`
- `apt install libsaxonb-java`

### Lancement de la transformation

**Il ne doit pas y avoir d'espaces ni de caractères spéciaux dans les noms des fichiers de données pour que le traitement soit automatisable.**

```bash
sh generate_html.bash
```

## Mise à jour des données à distance

**! Se placer dans la branche `updateData`.**

La chaîne d'intégration continue se lancera automatiquement si un commit entraîne une modification dans le dossier `data`. Il reste cependant de petites étapes manuelles à effectuer pour valider la transformation des données :

1. Les deux premiers jobs `xml-validation` et  `pages` se lancent à la suite de façon automatique. Il n'y aura donc qu'à vérifier que le résultat convient en se rendant ici : <https://gricad-gitlab.univ-grenoble-alpes.fr/elan/fnvr>.
2. Si c'est le cas, il faut alors bien penser à lancer le troisième job `update-data` manuellement pour que la transformation des données soit mise à jour dans le dépôt.
3. Une fois que c'est fait, la branche `updateData` peut être mergée dans la branche `main`.
4. Il faudra bien penser à faire un `git pull` de chacune de ces deux branches en local pour récupérer les données mises à jour avant de continuer le traitement.

## Auteurs

Voir [le fichier AUTHOR](AUTHOR)

## Licence

GNU AFFERO GENERAL PUBLIC LICENSE Version 3 (voir [le fichier LICENSE](LICENSE))
