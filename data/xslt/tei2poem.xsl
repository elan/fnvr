<?xml version="1.0" encoding="UTF-8"?><!--
/**

* XSLT for transformation of TEI to HTML : poem
* @author RachelGaubil@UGA
* @date : 2023
*/
-->

<!DOCTYPE tei2poem [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs tei" version="2.0">

    <xsl:output method="html" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>

    <!--Enlève les espaces superflus?-->
    <xsl:strip-space elements="*"/>

    <!-- Template qui s'applique à la racine du XML -->
    <xsl:template match="/">

        <xsl:variable name="poem_author">
            <xsl:call-template name="generate_author"/>
        </xsl:variable>

        <xsl:variable name="poem_title">
            <xsl:call-template name="generate_title"/>
        </xsl:variable>

        <xsl:for-each select="tei:TEI/tei:text">
            <xsl:variable name="text" select="current()"/>

            <!--<xsl:variable name="poem_nb">
                <xsl:value-of select="count(preceding::tei:text) + 1"/>
            </xsl:variable>-->
            <!--<xsl:variable name="poem_id">
                <xsl:value-of select="substring-after(@xml:id, '_')"/>
            </xsl:variable>-->

            <html lang="fr">
                <head>
                    <meta charset="utf-8"/>
                    <meta name="viewport" content="width=device-width, initial-scale=1"/>
                    <title>
                        <xsl:text>FNVR - </xsl:text>
                        <xsl:value-of select="$poem_title"/>
                    </title>
                    <link rel="stylesheet" href="../assets/css/lib/bootstrap.min.css"/>
                    <link rel="stylesheet"
                        href="../assets/css/lib/fontawesome/css/fontawesome.min.css"/>
                    <link href="../assets/css/lib/fontawesome/css/solid.min.css" rel="stylesheet"/>
                    <link rel="stylesheet" href="../assets/css/main.css"/>
                    <link rel="stylesheet" href="../assets/css/poem.css"/>
                    <link rel="shortcut icon" type="image/x-icon" href="../assets/img/feather.png"/>
                </head>

                <body>
                    <!-- menu -->
                    <nav class="navbar navbar-expand-lg bg-light sticky-top">
                        <div class="container-fluid">
                            <div class="collapse show navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav me-auto mx-auto">
                                    <li class="nav-item">
                                        <!-- index.html si accueil sinon ../index.html -->
                                        <a class="btn btn-menu me-3" href="../index.html">
                                            <i class="fa-solid fa-house"/>
                                            <xsl:text> Accueil</xsl:text>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="btn btn-menu me-3" href="./texts.html">
                                            <i class="fa-solid fa-book-open"/> Textes</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="btn btn-menu me-3"
                                            href="./transcriptionRules.html">
                                            <i class="fa-solid fa-file-pen"/> Principes de
                                            transcription</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>

                    <div class="container-fluid">
                        <div class="content row justify-content-center">
                            <div class="col-9">
                                <h3 class="title">
                                    <!--Fictions narratives en vers de la Renaissance-->
                                    <xsl:if test="$poem_author != ''">
                                        <span class="author">
                                            <xsl:value-of select="$poem_author"/>
                                        </span>
                                    </xsl:if>
                                    <span class="poem-title">
                                        <xsl:value-of select="$poem_title"/>
                                    </span>
                                </h3>

                                <xsl:call-template name="generate_pageTitle">
                                    <xsl:with-param name="text" select="$text" tunnel="yes"/>
                                </xsl:call-template>

                                <div class="content">
                                    <xsl:call-template name="generate_content">
                                        <xsl:with-param name="text" select="$text" tunnel="yes"/>
                                    </xsl:call-template>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="note" tabindex="-1" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <h3>
                                            <span id="note-title-id"/>
                                        </h3>
                                        <div class="note-content"> </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-modal"
                                            data-bs-dismiss="modal">Fermer</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>
                    </div>
                    <script type="text/javascript" src="../assets/js/lib/bootstrap.bundle.min.js"/>
                    <script type="text/javascript" src="../assets/js/poem.js"/>
                </body>
            </html>

        </xsl:for-each>

    </xsl:template>


    <xsl:template name="generate_author">
        <!--<xsl:variable name="author">
            <xsl:value-of select="tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author"/>
        </xsl:variable>-->
        <xsl:if test="tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author != ''">
            <!--si besoin d'inverser prénom-nom
            <xsl:variable name="lastname">
                <xsl:value-of select="tokenize($author, ' ')[last()]"/>
            </xsl:variable>
            <xsl:variable name="firstname">
                <xsl:value-of select="substring-before($author, $lastname)"/>
            </xsl:variable>
            <!-\-Enlever l'espace à la fin-\->
            <xsl:variable name="firstname_clear">
                <xsl:value-of select="substring($firstname, 1, string-length($firstname)-1)"/>
            </xsl:variable>
            <xsl:value-of select="concat($lastname, ', ', $firstname_clear, ', ')"/>-->
            <xsl:value-of select="tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author"/>
            <xsl:text>, </xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template name="generate_title">
        <xsl:value-of select="tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/>
    </xsl:template>

    <!-- Create poem view -->

    <xsl:template name="generate_pageTitle">
        <xsl:param name="text" required="yes" tunnel="yes"/>
        <xsl:for-each select="./tei:front">
            <div class="pageTitle">
                <xsl:apply-templates mode="pageTitle"/>
            </div>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="tei:head" mode="pageTitle">
        <h4>
            <xsl:if test="@type = 'main' or not(@*)">
                <span class="margin-text">Page de titre</span>
            </xsl:if>
            <xsl:apply-templates/>
        </h4>
    </xsl:template>
    <xsl:template match="tei:titlePart" mode="pageTitle">
        <h4>
            <xsl:if test="@type = 'main' or not(@*)">
                <span class="margin-text">Page de titre</span>
            </xsl:if>
            <xsl:apply-templates/>
        </h4>
    </xsl:template>
    <xsl:template match="tei:imprimatur" mode="pageTitle">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="tei:docImprint" mode="pageTitle">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <!--<xsl:template match="tei:front//tei:head[@type = 'main' or not(@*)]" mode="#all">
        <h3>
            <xsl:apply-templates mode="#current"/>
        </h3>
    </xsl:template>
    
    <xsl:template match="tei:front//tei:head[@type = 'sub']" mode="#all">
        <h4>
            <xsl:apply-templates mode="#current"/>
        </h4>
    </xsl:template>-->

    <xsl:template name="generate_content">
        <xsl:param name="text" required="yes" tunnel="yes"/>

        <xsl:for-each select="./tei:body">

            <xsl:choose>
                <xsl:when test="ancestor::tei:text[1]/@xml:id = 'poem_rondeaux'">
                    <xsl:apply-templates mode="rondeaux"/>
                </xsl:when>
                <xsl:when test="ancestor::tei:text[1]/@xml:id = 'poem_amantDesconforte'">
                    <xsl:apply-templates mode="amantDesconforte"/>
                </xsl:when>
                <xsl:when test="ancestor::tei:text[1]/@xml:id = 'poem_chevallier'">
                    <xsl:apply-templates mode="chevallier"/>
                </xsl:when>
                <xsl:when test="ancestor::tei:text[1]/@xml:id = 'poem_eurial'">
                    <xsl:apply-templates mode="eurial"/>
                </xsl:when>
                <!-- palamon, eurialus-->
                <xsl:otherwise>
                    <xsl:apply-templates mode="others"/>
                </xsl:otherwise>
            </xsl:choose>

            <!-- exclure les notes vers latins -->
            <xsl:if test="count($text//tei:note[not(@place)]) > 0">
                <xsl:call-template name="create_notes">
                    <xsl:with-param name="text" select="$text" tunnel="yes"/>
                </xsl:call-template>
            </xsl:if>

        </xsl:for-each>
    </xsl:template>

    <xsl:template name="create_notes">
        <xsl:param name="text" required="yes" tunnel="yes"/>
        <div class="notes">
            <!-- exclure les notes vers latins -->
            <xsl:apply-templates mode="note" select="$text//tei:note[not(@place)]"/>
        </div>
    </xsl:template>

    <xsl:template name="chapNb">
        <!-- Ajouter le numéro de chapitre -->
        <xsl:if test="ancestor::tei:div[1][@type = 'chap']">
            <span class="chapNb">
                <xsl:value-of select="ancestor::tei:div[1]/@n"/>
            </span>
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>

    <!--cas particuliers : eurial et eurialus-->
    <xsl:template match="tei:div" mode="#all">
        <div>
            <xsl:choose>
                <xsl:when test="@type = 'péritexte'">
                    <xsl:attribute name="class">
                        <xsl:text>strophe fw-bold</xsl:text>
                    </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="class">
                        <xsl:text>strophe</xsl:text>
                    </xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>

            <!--récup les mots automatiquement + maj sur première lettre ? Eviterai les when-->
            <xsl:if test="@type != 'chap' and @resp">
                <span class="margin-text">
                    <xsl:choose>
                        <xsl:when test="@type = 'dédicace'">
                            <xsl:text>Dédicace</xsl:text>
                        </xsl:when>
                        <xsl:when test="@type = 'prologue'">
                            <xsl:text>Prologue</xsl:text>
                        </xsl:when>
                        <!--chevallier : épilogue mais eurial paratexte-->
                        <xsl:when test="(@type = 'paratexte' and @resp) or @type = 'épilogue'">
                            <xsl:text>Epilogue</xsl:text>
                        </xsl:when>
                        <!--<xsl:when test="@type = 'péritexte'">
                            <span class="margin-text">Colophon</span>
                        </xsl:when>-->
                    </xsl:choose>
                    <xsl:variable name="paratextNoAuthorNb">
                        <xsl:value-of select="count(//tei:div[@resp != 'auteur'])"/>
                    </xsl:variable>
                    <xsl:variable name="paratextNb">
                        <xsl:value-of select="count(//tei:div[@type != 'chap' and @resp])"/>
                    </xsl:variable>
                    <xsl:if test="$paratextNoAuthorNb > 0 and $paratextNb > 1">
                        <xsl:text> de l'</xsl:text>
                        <xsl:value-of select="@resp"/>
                    </xsl:if>
                </span>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </div>
    </xsl:template>

    <!-- si pas de type de titre alors titre principal (pas sous-titre) or tei:div[@type='chap' or @type='prologue' or @type='dédicace']/tei:head[@type='main' or not(@*)] -->
    <xsl:template match="tei:body//tei:head[@type = 'main' or not(@*)]" mode="#all">
        <xsl:variable name="chapNum">
            <xsl:call-template name="chapNb"/>
        </xsl:variable>
        <h5>
            <xsl:if test="$chapNum = '[1] '">
                <xsl:attribute name="class">firstChap</xsl:attribute>
            </xsl:if>
            <!--exclure les head contenant des vers (traités au niveau des vers)-->
            <xsl:if test="not(child::tei:l)">
                <xsl:copy-of select="$chapNum"/>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </h5>
    </xsl:template>

    <!-- doublon avec précédent, ne sera nécessaire que si "plus grand espace entre péritexte et premier chapitre" valable pour tous et pas qu'Eurial (donc mettre mode='eurial' à précedent) -->
    <!--<xsl:template match="tei:body//tei:head[@type = 'main' or not(@*)]" mode="rondeaux amantDesconforte chevallier others">
        <h5>
            <xsl:call-template name="chapNb"/>
            <xsl:apply-templates mode="#current"/>
        </h5>
    </xsl:template>-->

    <xsl:template match="tei:body//tei:head[@type = 'sub']" mode="#all">
        <h6>
            <xsl:apply-templates mode="#current"/>
        </h6>
    </xsl:template>

    <xsl:template match="tei:lg" mode="#all">
        <p>
            <xsl:if test="@type = 'refrain'">
                <xsl:attribute name="class">
                    <xsl:text>indent</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </p>
    </xsl:template>

    <!-- numérotation au changement de chapitre -->
    <!--<xsl:template match="tei:l" mode="rondeaux eurial amantDesconforte">-->
    <xsl:template match="tei:l" mode="rondeaux eurial">
        <xsl:variable name="div-type" select="ancestor::tei:div[1]/@type"/>

        <span>
            <xsl:choose>
                <xsl:when
                    test=". = parent::tei:lg[@type != 'refrain' or not(@type)]/tei:l[1] and text() != '………………………………………'">
                    <xsl:attribute name="class">verse mini-indent</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="class">verse</xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>

            <xsl:if test="$div-type != 'péritexte'">
                <!-- numérotation recommence quand le chapitre change -->
                <xsl:variable name="verseNb">
                    <xsl:number count="tei:l" level="any" from="tei:div"/>
                </xsl:variable>
                <xsl:if test="$verseNb and xs:integer($verseNb mod 5) = 0">
                    <span class="verse-nb">
                        <xsl:value-of select="$verseNb"/>
                    </span>
                </xsl:if>
                <xsl:if test="not(ancestor::tei:div[1][@type = 'chap']/tei:head) and $verseNb = 1">
                    <xsl:call-template name="chapNb"/>
                </xsl:if>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>

    <!-- à faire vérifier si on veut vraiment tout numéroter à la suite (cf. word) et pas juste le récit comme consigne, si séparation remettre amant avec rondeaux et eurial ci-dessus -->
    <xsl:template match="tei:l" mode="amantDesconforte">
        <xsl:variable name="div-type" select="ancestor::tei:div[1]/@type"/>

        <span>
            <xsl:choose>
                <xsl:when
                    test=". = parent::tei:lg[@type != 'refrain' or not(@type)]/tei:l[1] and text() != '………………………………………'">
                    <xsl:attribute name="class">verse mini-indent</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="class">verse</xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>

            <!-- numérotation recommence quand le chapitre change -->
            <xsl:variable name="verseNb">
                <xsl:number count="tei:l" level="any"/>
            </xsl:variable>
            <xsl:if test="$verseNb and xs:integer($verseNb mod 5) = 0">
                <span class="verse-nb">
                    <xsl:value-of select="$verseNb"/>
                </span>
            </xsl:if>

            <xsl:variable name="verseCounter">
                <xsl:number count="tei:l" level="any" from="tei:div"/>
            </xsl:variable>
            <xsl:if test="not(ancestor::tei:div[1][@type = 'chap']/tei:head) and $verseCounter = 1">
                <xsl:call-template name="chapNb"/>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>

    <!-- mettre le dernier vers de la dédicace qui en retrait -->
    <!-- pb aurialus et palamon entrent ici au lieu d'aller dans others-->
    <xsl:template match="tei:l" mode="chevallier">

        <xsl:variable name="div-type" select="ancestor::tei:div[1]/@type"/>
        <span>
            <xsl:choose>
                <xsl:when test="ancestor::tei:div[1 and @type = 'dédicace'] and position() = last()">
                    <xsl:attribute name="class">verse indent</xsl:attribute>
                </xsl:when>
                <xsl:when
                    test=". = parent::tei:lg[@type != 'refrain' or not(@type)]/tei:l[1] and text() != '………………………………………'">
                    <xsl:attribute name="class">verse mini-indent</xsl:attribute>
                </xsl:when>
                <xsl:when
                    test="parent::tei:head[not(@*)] and . = parent::tei:head[not(@*)]/child::*[1]">
                    <xsl:attribute name="class">verse</xsl:attribute>
                    <xsl:call-template name="chapNb"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="class">verse</xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>

            <!-- ne pas numéroter les péritextes ni les titres -->
            <xsl:if test="$div-type != 'péritexte' and name(parent::*[1]) != 'head'">
                <!-- numérotation recommence quand le type de chapitre change -->
                <xsl:variable name="verseNb">
                    <xsl:number count="tei:l[name(parent::*[1]) != 'head']" level="any"
                        from="tei:div[not(@type = 'chap') or @n = '[1]']"/>
                </xsl:variable>
                <xsl:if test="$verseNb and xs:integer($verseNb mod 5) = 0">
                    <span class="verse-nb">
                        <xsl:value-of select="$verseNb"/>
                    </span>
                </xsl:if>
                <xsl:if test="not(ancestor::tei:div[1][@type = 'chap']/tei:head) and $verseNb = 1">
                    <xsl:call-template name="chapNb"/>
                </xsl:if>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:l" mode="others">
        <xsl:variable name="div-type" select="ancestor::tei:div[1]/@type"/>

        <span>
            <xsl:choose>
                <xsl:when
                    test=". = parent::tei:lg[@type != 'refrain' or not(@type)]/tei:l[1] and text() != '………………………………………'">
                    <xsl:attribute name="class">verse mini-indent</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="class">verse</xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>

            <xsl:if test="$div-type != 'péritexte'">
                <!-- numérotation recommence quand le type de chapitre change -->
                <xsl:variable name="verseNb">
                    <xsl:number count="tei:l" level="any"
                        from="tei:div[(not(@type = 'chap') and not(@type = 'rondeau')) or @n = '[1]']"
                    />
                </xsl:variable>
                <xsl:if test="$verseNb and xs:integer($verseNb mod 5) = 0">
                    <span class="verse-nb">
                        <xsl:value-of select="$verseNb"/>
                    </span>
                </xsl:if>
                <xsl:if test="not(ancestor::tei:div[1][@type = 'chap']/tei:head) and $verseNb = 1">
                    <xsl:call-template name="chapNb"/>
                </xsl:if>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>

    <!--<xsl:template match="tei:fw" mode="#all">
        <span class="folio">
            <xsl:value-of select="@n"/>
        </span>
    </xsl:template>-->
        
    <xsl:template match="tei:fw" mode="rondeaux amantDesconforte eurial chevallier others">
        
        <xsl:variable name="num">
            <xsl:value-of select="count(preceding::tei:note[not(@place)])+1"/>
        </xsl:variable>
        
        <span class="folio">
            <xsl:value-of select="@n"/>
            
            <!--ne pas traiter la note du folio au niveau du texte-->
            <xsl:if test="parent::*[1]/name() != 'l' and following-sibling::*[1]/name() = 'note'">
                
                <!-- récupère le texte entre le folio et la note suivante-->
                <xsl:variable name="start" select="following-sibling::tei:note[1]"/>
                <xsl:variable name="startText" select="following::text()[$start and generate-id(following::tei:note[1]) = generate-id($start)]"/>
                
                <!--si la note concerne le folio et pas le texte qui la précède-->
                <xsl:if test="1 >= string-length($startText)">
                    <sup class="modal-call note-call" aria-hidden="true" id="noteCall{$num}"
                        data-modal-id="n{$num}">
                        <!--Add css change cursor -->
                        <xsl:value-of select="$num"/>
                    </sup>
                </xsl:if>
            </xsl:if>
            
        </span>
    </xsl:template>

    <!-- si eurial et eurialus aussi -->
    <!--<xsl:template match="tei:lb" mode="#all">-->
    <xsl:template match="tei:lb" mode="amantDesconforte">
        <xsl:value-of disable-output-escaping="yes">&lt;br/></xsl:value-of>
    </xsl:template>

    <!-- vers en latin -->
    <xsl:template match="tei:note[@place = 'margin_right']"
        mode="rondeaux amantDesconforte eurial chevallier others">
        <span class="latin-verse">
            <xsl:apply-templates mode="#current"/>
        </span>
    </xsl:template>

    <xsl:template match="tei:note[not(@place)]"
        mode="rondeaux amantDesconforte eurial chevallier others">
        <xsl:variable name="num">
            <xsl:number count="tei:note[not(@place)]" level="any"/>
        </xsl:variable>
        
        <xsl:choose>
            <!--ne pas traiter la note du folio au niveau du texte-->
            <xsl:when test="parent::*[1]/name() != 'l' and preceding-sibling::*[1]/name() = 'fw'">
               
               <!-- récupère le texte entre le folio précédent et la note actuelle-->
               <xsl:variable name="end" select="preceding-sibling::tei:fw[1]"/>
               <xsl:variable name="endText" select="preceding::text()[$end and generate-id(preceding::tei:fw[1]) = generate-id($end)]"/>
                
                <!--si la note concerne le texte après le folio et pas le folio lui-même-->
                <xsl:if test="string-length($endText) > 1">
                    <sup class="modal-call note-call" aria-hidden="true" id="noteCall{$num}"
                        data-modal-id="n{$num}">
                        <!--Add css change cursor -->
                        <xsl:value-of select="$num"/>
                    </sup>
               </xsl:if>
            </xsl:when>
            
            <xsl:otherwise>
                <sup class="modal-call note-call" aria-hidden="true" id="noteCall{$num}"
                    data-modal-id="n{$num}">
                    <!--Add css change cursor -->
                    <xsl:value-of select="$num"/>
                </sup>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:note" mode="note">
        <xsl:param name="text" required="yes" tunnel="yes"/>
        <xsl:variable name="text-id" select="generate-id($text)"/>
        <xsl:variable name="num">
            <xsl:number
                count="tei:note[not(@place) and generate-id(ancestor::tei:text[1]) = $text-id]"
                level="any"/>
        </xsl:variable>
        <p class="note-title" note-title="n{$num}">Variante n°<xsl:value-of select="$num"/></p>
        <p class="note-content" note-content="n{$num}">
            <xsl:apply-templates mode="#current"/>
        </p>
    </xsl:template>

    <xsl:template match="tei:stage[@type = 'setting']" mode="#all">
        <p class="pers mini-indent">
            <xsl:apply-templates mode="#current"/>
        </p>
    </xsl:template>

    <!-- A RETIRER SI ON CHOISIT DE L'ENCODER -> à faire en fonction de ce que c'est, dommage ici car hyper spécifique -->
    <xsl:template match="tei:p" mode="amantDesconforte">
        <xsl:if test="position() = last()">
            <span class="margin-text">
                <xsl:text>Colophon</xsl:text>
            </span>
        </xsl:if>
        <xsl:apply-templates mode="#current"/>
    </xsl:template>

    <!-- IDEM : A RETIRER MAIS SI ON CHOISIT DE TRAITER LES P, dommage ici car hyper spécifique -->
    <xsl:template match="tei:p" mode="eurial">
        <p>
            <xsl:if test=". = parent::*[1]/tei:p[1] and position() != last()">
                <xsl:attribute name="class">
                    <xsl:text>mini-indent</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <!-- si pas de titre alors on ajoute le numéro de chapitre devant le paragraphe-->
            <xsl:if test="not(preceding-sibling::tei:head)">
                <span class="chapNb">
                    <xsl:value-of select="parent::tei:div/@n"/>
                </span>
                <xsl:text> </xsl:text>
            </xsl:if>
            <xsl:apply-templates mode="#current"/>
        </p>
    </xsl:template>

    <xsl:template match="tei:hi[@rend = 'sup']" mode="#all">
        <sup>
            <xsl:apply-templates mode="#current"/>
        </sup>
    </xsl:template>

    <!-- MODES
        pour l'instant 'rondeaux' et 'eurial' et 'amant' suivent le mêmes consignes en numerotation de vers
    -->

</xsl:stylesheet>
