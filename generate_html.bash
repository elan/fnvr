#!/bin/bash

log="generate_html.log"
echo "Log "`date` > $log

echo "# Création des sorties html pour les fictions narratives" | tee -a $log
xsl="data/xslt/tei2poem.xsl"
echo "  XSL: "$xsl

inputDir="data/tei/*"
outputDir="templates/"

# Parcours des fichiers à transformer
for inputFile in $inputDir
do
  # Création des chemins/noms des fichiers de sorties
  fileName=$(basename $inputFile ".xml")
  outputFile=$outputDir$fileName".html"
  # Transformation
  saxonb-xslt -o $outputFile $inputFile $xsl 2>>$log
  echo "  - Input:  "$inputFile
  echo "  - Output: "$outputFile
  echo
done
